package android.mindandmatters.midterm;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Grabbing all items
        ImageView iv = (ImageView) findViewById(R.id.emotion_picture);
        final EditText et = (EditText) findViewById(R.id.description);
        Button btn = (Button) findViewById(R.id.save_note);

        //Grabbing any saved data
        final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String data = sharedPreferences.getString("notes", "No Data");
        int emotionData = sharedPreferences.getInt("selected", 1);

        //Grabbing saved data and setting text.
        et.setText(data);

        //Open image gallery when image is clicked
        iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), ImageGallery.class);
                startActivity(intent);
            }
        });

        //listening for save note button
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String data = "";

                //Saving the data
                sharedPreferences.edit().putString("notes", et.getText().toString()).apply();
            }
        });


        switch (emotionData) {
            case 1:
                emotionData = 1;
                iv.setImageResource(R.drawable.happy);
                break;
            case 2:
                emotionData = 2;
                iv.setImageResource(R.drawable.sad);
                break;
            case 3:
                emotionData = 3;
                iv.setImageResource(R.drawable.lovingit);
                break;
            case 4:
                emotionData = 4;
                iv.setImageResource(R.drawable.rollingeyes);
                break;
            case 5:
                emotionData = 5;
                iv.setImageResource(R.drawable.silly);
                break;
            case 6:
                emotionData = 6;
                iv.setImageResource(R.drawable.crosseye);
                break;
            case 7:
                emotionData = 7;
                iv.setImageResource(R.drawable.scared);
                break;
            case 8:
                emotionData = 8;
                iv.setImageResource(R.drawable.thinking);
                break;

        }
    }
}
